<?php

	declare(strict_types=1);

class activity_model extends CI_Model {

    public function __construct()
    {
        $this->load->database();
    }

    public function get_activities_on_date($aData) {
        $query = $this->db->get_where('activity_tracker', array('date' => $aData));
        return $query->row_array();
    }

    public function get_moods_on_date($date) {
        $query = $this->db->get_where('mood_tracker', array('date' => $date));
        return $query->result_array();
    }
}