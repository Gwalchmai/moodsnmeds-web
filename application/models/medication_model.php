<?php
/**
 * Created by PhpStorm.
 * User: redlo
 * Date: 01/08/2017
 * Time: 18:52
 */

	declare(strict_types=1);

Class medication_model extends CI_model
{
    public function __construct()
    {
        $this->load->database();
    }

    public function get_medications($iMedicationID = 0)
    {
        if($iMedicationID === 0)
        {
            $query = $this->db->get('medication_details');
            return $query->result_array();
        }

        $query = $this->db->get_where('medication_details', array('id' => $iMedicationID));
        return $query->row_array();
    }

    public function add_medication()
    {
        $aData = array(
            'medication_name' => $this->input->post('medication_name'),
            'dosage_size' => $this->input->post('dosage_size'),
            'dosage_type' => $this->input->post('dosage_type'),
            'number_doses' => $this->input->post('number_doses')
        );
        return $this->db->insert('medication_details', $aData);
    }
}