    <div class="col-md-9">
        <h4><?= $sTitle; ?></h4>
        <table class="table table-hover">
            <tbody>
            <tr class="info">
                <th colspan="4">
                    Today's Inspiration
                </th>
            </tr>
            <tr>
                <td colspan="4">
                 <?php
                    if ($aData['activities']['inspiration'] != '')
                     echo $aData['activities']['inspiration'];
                     else echo 'No inspiration added for today';
                    ?>
                </td>
            </tr>
            <tr class="info">
                <th></th>
                <th>Priority</th>
                <th>Goal</th>
                <th>Achieved</th>
            </tr>
            <?php
                $count = 1;
                if (is_array($aData['goals']))
                    foreach ($aData['goals'] as $id => $goal) {
            ?>
            <tr>
                <td><?= $count ?></td>
                <td><?= $goal['priority'] ?></td>
                <td><?= $goal['goal'] ?></td>
                <td><?= $goal['achieved'] ?></td>
            </tr>
             <?php
                $count++;
            } else {
            ?>
            <tr>
                <td colspan="4">No goals added for today</td>
            </tr>
            <?php
            }
            ?>
            <tr class="info">
                <th colspan="4">Today I Learned</th>
            </tr>
            <tr>
                <td colspan="4">
                 <?php
                    if ($aData['activities']['learned_today'] != '') echo $aData['activities']['learned_today'];
                    else echo 'Nothing added yet';
                 ?>
                </td>
            </tr>
            <tr class="info">
                <th colspan="4">Today I Felt</th>
            </tr>
            <tr>
                <td colspan="4">
                 <?php if ($aData['activities']['felt_today'] != '') echo $aData['activities']['felt_today'];
                        else echo 'Nothing added yet'; ?>
                </td>
            </tr>
            <tr class="info">
                <th></th>
                <th>Priority</th>
                <th>Goal</th>
                <th>Reason Not Achieved</th>
            </tr>
            <?php
                $count = 1;
                $iDate = strtotime("Today");
                if (is_array($aData['goals']))
                    foreach ($aData['goals'] as $id => $goal) {
                        if ($goal['reason_not_achieved'] != '') {
            ?>
                <tr>
                    <td><?= $count ?></td>
                    <td><?= $goal['priority'] ?></td>
                    <td><?= $goal['goal'] ?></td>
                    <td><?= $goal['reason_not_achieved'] ?></td>
                </tr>
                <?php
                $count++;
            }
        } else {
        ?>
        <tr>
            <td colspan="4">No problems today</td>
        </tr>
        <?php
        }
        ?>
            <tr class="info">
                <td colspan="4" style="text-align: right;"><a href="/activities/edit/<?= $iDate ?>" class="btn btn-primary
                        btn-sm">Edit</a></td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
