<!DOCTYPE HTML>
<html>
    <head>
        <title>Moods 'n Meds</title>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/assets/css/styles.css">
    </head>
    <body>
    <nav class="navbar navbar-inverse">
        <div class="container">
            <a class="navbar-brand" href = "/"><h1>Moods 'n Meds</h1></a>
        </div>
        <div id="navbar">
            <ul class="nav navbar-nav">
                <li><a href="/">Home</a></li>
                <li><a href="/about">About</a></li>
                <li><a href="/medications">Medications</a></li>
                <li><a href="/activities">Activities</a></li>
            </ul>
        </div>
    </nav>
    <div class="container">
