<!DOCTYPE HTML>
<html>
<head>
<title> Moods 'n Meds</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.css">
	<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
<div class="container-fluid" style="max-width: 600px">
	<?php echo form_open("auth/login"); ?>
	<fieldset>
		<legend class="col-md-10 col-md-offset-2">Moods 'n Meds</legend>
		<p class="text-info col-md-10 col-md-offset-2">Please enter your email address and password to log in</p>
		<div class="text-danger col-md-10 col-md-offset-2"><?php echo $message; ?></div>
		<div class="form-group">
			<label for="identity" class="col-md-2 control-label">Email</label>
			<div class="col-md-10">
				<input class="form-control" id="identity" placeholder="Email" name="identity" type="text">
			</div>
		</div>

		<div class="form-group">
			<label for="password" class="col-md-2 control-label">Password</label>
			<div class="col-md-10">
				<input class="form-control" id="password" placeholder="Password" name="password" type="password">
			</div>
		</div>

		<div class="checkbox col-md-10 col-md-offset-2">
			<label>
				<input type="checkbox" name="remember" value="1"> Remember Me
			</label>
		</div>

		<div class="form-group">
			<div class="col-md-10 col-md-offset-2">
				<input type="submit" class="btn btn-default" name="Submit" value="submit">
			</div>
		</div>
	</fieldset>
	<?php echo form_close(); ?>
	<a href="forgot_password" class="btn btn-link col-md-offset-2"><?php echo lang('login_forgot_password'); ?></a>
</div>
</body>
</html>