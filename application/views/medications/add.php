<h2><?= $title ?></h2>

<?php echo validation_errors(); ?>

<?php echo form_open('medications/add'); ?>
    <div class="form-group">
        <label>Medication Name:</label>
        <input type="text" name="medication_name" class="form-control" placeholder="Medication">
    </div>
    <div class="form-group">
        <label>Dose per tablet:</label>
        <input type="text" name="dosage_size" class="form-control" placeholder="Dosage">
    </div>
    <div class="form-group">
        <label>Dose Type</label>
        <select name="dosage_type">
            <option>Select a Dose Type</option>
            <option>mg</option>
            <option>ug</option>
            <option>puffs</option>
            <option>ml</option>
        </select>
    </div>
    <div class="form-group">
        <label>Number of Doses:</label>
        <input type="text" name="number_doses" class="form-control" placeholder="Number of doses">
    </div>
    <div class="checkbox">
        <label>
            <input type="checkbox" name="active" value="1"> Active
        </label>
    </div>
    <button type="submit" class="btn btn-default">Submit</button>
</form>