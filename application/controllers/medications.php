<?php
/**
 * Created by PhpStorm.
 * User: redlo
 * Date: 01/08/2017
 * Time: 06:06
 */

declare(strict_types=1);

class Medications extends CI_Controller
{

    public function index()
    {

        $aData['title'] = "Medication List";

        $aData['medications'] = $this->medication_model->get_medications();

        $this->load->view('/templates/header');
        $this->load->view('/templates/left_navigation');
        $this->load->view('/medications/index', $aData);
        $this->load->view('/templates/footer');
    }

    public function medicationDetails($iMedicationID = 0)
    {
        $data['medication'] = $this->medication_model->get_medications($iMedicationID);

        if (empty($aData['medication'])) {
            show_404();
        }

        $data['title'] = $aData['medication']['medication_name'];

        $this->load->view('/templates/header');
        $this->load->view('/medications/detail', $aData);
        $this->load->view('/templates/footer');

    }

    public function add()
    {
        $aData['title'] = 'Add Medication';

        $this->form_validation->set_rules('medication_name', 'Medication', 'required');
        $this->form_validation->set_rules('number_doses', 'Number of Doses', 'required');
        $this->form_validation->set_rules('dosage_size', 'Size of Dose', 'required');
        if ($this->form_validation->run() === FALSE) {
            $this->load->view('/templates/header');
            $this->load->view('/medications/add', $aData);
            $this->load->view('/templates/footer');
        } else {
            $this->medication_model->add_medication();
            redirect('medications');
        }


    }
}