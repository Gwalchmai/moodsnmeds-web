<?php
/**
 * Created by PhpStorm.
 * User: redlo
 * Date: 19/08/2017
 * Time: 21:14
 */

class Activities extends CI_Controller {

    public function index($iDate = 0) {

				    if ($iDate == 0) $iDate = strtotime("today");
				    $sDate = date("j F, Y", $iDate);
        $aData['sTitle'] = "Activities on  $sDate";

        $aData['aActivates'] = $this->activity_model->get_activities_on_date($iDate);
        $aData['aMoods'] = $this->activity_model->get_moods_on_date($iDate);

        $this->load->view('/templates/header');
        $this->load->view('/templates/left_navigation');
        $this->load->view('/activities/index', $aData);
        $this->load->view('/templates/footer');
    }

    public function edit_activity ($iDate = 0) {

				    if ($iDate == 0) $iDate = strtotime("today");
				    $sDate = date("Y-m-d", $iDate);
				    $aData['title'] = "Add/Edit Activities for $sDate";

				    $aData['aActivaties'] = $this->activity_model->get_activities_on_date($iDate);
				    $aData['aMoods'] = $this->activity_model->get_moods_on_date($iDate);

				    $this->load->view('/templates/header');
				    $this->load->view('/templates/left_navigation');
				    $this->load->view('/activities/edit', $aData);
				    $this->load->view('/templates/footer');
					}
}